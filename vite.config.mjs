/** @type {import('vite').UserConfig} */
import commonjs from '@rollup/plugin-commonjs';
import { resolve } from 'path';
import { defineConfig } from 'vite';
export default defineConfig({
  plugins: [commonjs()],
  build: {
    lib: {
      // Could also be a dictionary or array of multiple entry points
      entry: resolve(__dirname, 'src/index.js'),
      name: 'jscad2-regl-renderer',
      // the proper extensions will be added
      fileName: 'index'
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: [
        '@jscad/csg',
        '@jscad/scad-api',
        'gl',
        'angle-normals',
        'camera-unproject',
        'gl',
        'gl-mat4',
        'gl-vec3',
        'glslify',
        'pngjs',
        'regl'
      ],
      output: {
        format: 'esm',
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          '@jscad/csg': 'jsCadCSG',
          '@jscad/scad-api': 'scadApi',
          '@jwc/jscad2-img-utils': '@jwc/jscad2-img-utils',
          'regl': 'wrapREGL',
          'camera-unproject': 'unprotect',
        }
      }
    }
  }
});
